import java.util.Scanner;

public class TypeOfArray {
    public static void main(String[] args) {
        int noOfElements;
        Scanner scanner = new Scanner(System.in);
        noOfElements = scanner.nextInt();
        int[] arrayElements = new int [noOfElements];
        for(int index = 0; index < noOfElements; index++) {
            arrayElements[index] = scanner.nextInt();
        }
        int oddCount = 0, evenCount = 0;
        for(int i = 0 ; i < noOfElements; i++)  {
            if (arrayElements[i] % 2 == 1)
                oddCount++;
            else
                evenCount++;
        }
        if (oddCount == noOfElements)
            System.out.print("Odd");
        else if (evenCount == noOfElements)
            System.out.print("Even");
        else
            System.out.print("Mixed");
    }
}
